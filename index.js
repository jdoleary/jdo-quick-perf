const {performance} = require('perf_hooks')
module.exports = function (title){
    this.title = title
    this.lastTime = performance.now()
    this.lastEvent = 'Start'
    this.log = function(event){
        const currentTime = performance.now()
        console.log(`${title} | '${this.lastEvent}' to '${event}' took ${currentTime-this.lastTime} millis`)
        this.lastEvent = event
        this.lastTime = currentTime
    }
}
