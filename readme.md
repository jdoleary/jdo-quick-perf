# jdo-quick-perf

Log performance super easily and quickly!

``` js
const quickPerf = require('./index')

async function hefty(){
    const qp = new quickPerf('Hefty Function')
    await new Promise(res => setTimeout(res,2000))
    qp.log('timeout') // Output: Hefty Function | 'Start' to 'timeout' took 2003.883863999974 millis
    for(let i = 0; i < 1000000; i++){}
    qp.log('loop')  // Output: Hefty Function | 'timeout' to 'loop' took 18.04938800004311 millis
}

hefty()
```